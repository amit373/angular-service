import { Component } from '@angular/core';
import { LoginRsp } from './components/auth/auth.model';
import { Router } from '@angular/router';
import { AuthService } from './components/auth/auth.service';
import { Role } from './shared/models/userRole.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: LoginRsp;

  constructor(
      private router: Router,
      private authService: AuthService
  ) {
      this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isAdmin() {
      return this.currentUser && this.currentUser.userType === Role.Admin;
  }

  logout() {
      this.authService.logout();
      this.router.navigate(['/auth/login']);
  }
}
