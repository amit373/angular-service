import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import { StorageService } from './storage.service';

@Injectable()
export class ShareDataService {
  constructor(private storageService: StorageService) {}
  static toFixedDown(integer: number, digits: number = 0) {
    const numberString = (integer || 0).toFixed(10);
    // tslint:disable-next-line: one-variable-per-declaration
    const re = new RegExp('(\\d+\\.\\d{' + digits + '})(\\d)'), m = numberString.match(re);
    const result = m ? parseFloat(m[1]) : parseFloat(numberString).valueOf();
    return integer >= 0 ? result : (-1 * result);
  }

  static countDecimals(value: any) {
    const match = ('' + Number(value)).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    if (!match) {
      return 0;
    }
    return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
  }

  onKeyPressAllowNumbers(e: any, val: any) {
    if ([46, 8, 9, 27, 13, 110].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      if (e.keyCode === 46 && val.indexOf('.') === -1) {
        return;
      }
    }
    // Ensure that it is a number and stop the keypress & do not allow multiple points
    if (e.keyCode < 48 || e.keyCode > 57) {
      e.preventDefault();
    }
  }

  getErrorMessage(err: any) {
    let msg = 'Something Went Wrong';

    if (err.status === 401) {
      console.warn('DB cleared after 401 error');
      this.storageService.resetStorage();
      return msg;
    }

    if (err.status === 500) {
      return msg;
    }

    if (err.error) {
      delete err.error.status;
      if (err.error.message) {
        msg = err.error.message;
      } else if (_.isArray(err.error[Object.keys(err.error)[0]])) {
        msg = err.error[Object.keys(err.error)[0]][0];
      } else {
        msg = err.error[Object.keys(err.error)[0]];
      }
    } else if (err.message) {
      msg = err.message;
    } else if (_.isArray(err[Object.keys(err)[0]])) {
      msg = err[Object.keys(err)[0]][0];
    } else if (err.data && typeof err.data === 'string') {
      msg = err.data;
    } else if (err[Object.keys(err)[0]]) {
      msg = err[Object.keys(err)[0]];
    }

    if (msg === 'true' || typeof msg !== 'string') {
      if (err.status && err.status >= 200 && err.status < 300) {
        msg = 'success';
      } else {
        msg = 'Something Went Wrong';
      }
    }

    return msg;
  }
}
