import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Module
import { AuthRoutingModule } from './auth-routing.module';
// Components
import { AuthComponent } from './auth.component';
import { LoginComponent } from 'src/app/components/auth/login/login.component';
// Services
import { AuthService } from './auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AuthComponent, LoginComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthService]
})
export class AuthModule { }
